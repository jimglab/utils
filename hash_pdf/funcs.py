import h5py
import numpy as np
from numpy import (
    log10, array, ones, zeros, nan
)
import os, sys
from glob import glob
# for hash generation/encoding
from Crypto.Cipher import AES
import base64
# for multiple-page pdf generation
from matplotlib.backends.backend_pdf import PdfPages
# para hacer tablas, y transformarlas en .tex
from tabulate import tabulate
# para mergear .pdf files
from PyPDF2 import PdfFileMerger, PdfFileReader


def DecodeHex_and_GetIDs(fname_key=None):
    """
    Get the list of id-file-numbers from a 
    string stored inside the file 'fname_key'.
    IMPORTANT: this should be upated with the
    codification method implemented in
    the GenAnalysis::gen_codification() routine.
    """
    f = open(fname_key, 'r')
    key = f.readline()
    prefix = f.readline()[:-1] # avoiding the '\n'
    gh = GenHash()
    decoded_str = gh.decode(encoded=key)
    #--- here is the decoding method
    #n = len(decoded_str)/4
    #IDs = []
    #for i in range(n):
    #    IDs += [ int(decoded_str[4*i:4*(i+1)]) ]
    M = int(decoded_str)
    m = 10 # NOTE: this MUST BE the same as 
           # in GenAnalysis::gen_codification() !!
    n, IDs, flag = 0, [], True
    while flag:
        an = int( M/(2**(n*m)) )%(2**m) 
        if an==0:
            flag = False
        else:
            IDs += [ an ]
            n += 1
    #-------------------------------
    return decoded_str, IDs, prefix


class GenHash(object):
    """
    Class to handle encoding/decoding of a string, using
    a fixed set of parameters.
    """
    # the block size for the cipher object; must be 16, 24, or 32 for AES
    BLOCK_SIZE = 32
    # padding character, so that the value encrypted is 
    # always multiple of 'BLOCK_SIZE'
    PADDING = '{'
    # one-liner to sufficiently pad the text to be encrypted
    pad = lambda self, s:\
        s + (self.BLOCK_SIZE - len(s) % self.BLOCK_SIZE) * self.PADDING
    # one-liners to encrypt/encode and decrypt/decode a string
    # encrypt with AES, encode with base64
    EncodeAES = lambda self, c, s:\
        base64.b64encode(c.encrypt(self.pad(s)))
    DecodeAES = lambda self, c, e:\
        c.decrypt(base64.b64decode(e)).rstrip(self.PADDING)

    def __init__(self, **kargs):
        """
        input:
        - secret: used for encryption
        routine:
        - create a "secret" key (used to encrypt the message/key)
        - create a cipher object (using the "secret")
        """
        # generate a random secret key (if not given as argument)
        #secret = kargs.get('secret', os.urandom(self.BLOCK_SIZE))
        secret = '___JimmyMasias__' # THIS MUST BE FIXED!!
        self.myhash = {}
        # create a cipher object using the random secret
        self.myhash['cipher'] = AES.new(secret)

    def encode(self, mykey):
        """ encode a string """
        self.myhash['key'] =  mykey

        self.myhash['encoded'] = self.EncodeAES(self.myhash['cipher'], mykey)
        print 'Encrypted string:\n', self.myhash['encoded']
        return self.myhash['encoded']

    def decode(self, **kargs):
        """ decode the encoded string """
        encoded = kargs.get('encoded', self.myhash.get('encoded',None))
        assert encoded is not None,\
            " ### ERROR ###: Need 'encode' string from somewhere!"
        decoded = self.DecodeAES(self.myhash['cipher'], encoded)
        print 'Decrypted string:\n', decoded
        return decoded


class GenAnalysis(object):
    def __init__(self, ps, **kargs):
        # list of number IDs
        self.idlist = ps['id']
        self.ps     = ps
        #---- other arguments
        self.fprefix = kargs.get('prefix', 'o_') # input fname prefix

    def gen_codification(self):
        """
        Generates a string code based on our
        list 'self.idlist' of id-numbers
        Returns a string.
        """
        # encode a string
        m  = 10 # (*)
        nf = len(self.idlist)
        M  = 0
        for n in range(nf):
            M += self.idlist[n]*2**(n*m)
        MyKey = str(M)
        # (*) so we can store list of numbers whose 
        #     values doesn't exceed 2**m, for each of them.
        return MyKey

    def gen_hash(self):
        gh = GenHash()
        # I want hash properties available
        self.myhash = gh.myhash
        # build string codification
        MyKey = self.gen_codification() #'This_passWD :P'
        return gh.encode(MyKey)

    def make_pdf(self):
        """ 
        * We need:
        - hash identifier 'self.myhash["encoded"]' (e.g. see 
        the self.gen_hash() routine).

        * What we do:
        Llama a los generadores de figuras 'GralPlot::plot_...()', y los
        pone uno en c/pagina de .pdf
        Antes de esto, genera una pagina q contiene la tabla de 
        simulation-parameters, y la pone antes de los plots.
        """
        fname_base = self.myhash['encoded'].encode('hex')[:16]

        # construimos la pagina de parametros! :-)
        PdfOk, fname_param = self.build_params_pdf(fname_base)
        print " ---> PdfOk: ", PdfOk 
        
        fname_out_tmp  = '_tmp_fig_' + fname_base + '.pdf'
        fname_out = self.ps['dir_dst'] + '/fig_' + fname_base + '.pdf'
        pdf_pages = PdfPages(fname_out_tmp)

        # TODO: reemplazar 'GralPlot' con otra clase q sea
        # un ejemplo simple y funcional. Por ej., q tenga
        # metodos plot_*() q retornen una parabola, un scatter plot
        # de puntos random, un histograma, etc; c/u siendo guardado
        # en una pagina separada con pdf_pages.savefig() como abajo.
        gp = GralPlot(self.ps, prefix=self.fprefix, check=None, check_all=None)

        #--- 1st page
        fig, ax = gp.plot__parabolic()
        pdf_pages.savefig(fig, bbox_inches='tight')
        close(fig)

        #--- 2nd page
        fig, ax = gp.plot__scatter()
        pdf_pages.savefig(fig, bbox_inches='tight')
        close(fig)

        #--- Write the PDF document to the disk
        pdf_pages.close()
        print " ---> we generated the temp-file: " + fname_out_tmp

        # ahora mergueamos los .pdf
        fnames_to_mergue = [fname_param, fname_out_tmp]
        merger = PdfFileMerger()
        for fname in fnames_to_mergue:
            merger.append(PdfFileReader(file(fname, 'rb')))

        merger.write(fname_out)
        print " -----> Mergueamos los .pdf aqui:\n " + fname_out 
        
        # clean temporary files
        fnames_gb = fnames_to_mergue + [self.fname_tab_base+'*']
        print " -----> eliminandos .pdf temporales: ", fnames_gb
        for fnm in fnames_gb:
            # TODO: cambiar "rm" para Windows
            os.system('rm '+fnm)

        #--- save code into an ASCII .key file (with my identifier)
        fname_key = self.ps['dir_dst'] + '/' + fname_base + '.key'
        # TODO: cambiar "echo" para Windows
        os.system('echo ' + self.myhash['encoded'] + ' > '+fname_key)
        os.system('echo ' + self.fprefix + ' >> '+fname_key)
        print " ---> saved key (and prefix) into:\n"+fname_key

    def build_params_pdf(self, fname_base):
        p_comm, p_diff = self.compare_psim() # dictionaries
        tbcomm = [['name', 'value']]
        tbdiff = []

        #--- primero, hacemos la tabla para los parametros en comun
        # primero las semillas
        for nm in p_comm.keys():
            if nm.startswith('sem_'):
                name = '\\texttt{%s}'%nm.replace('_', u'\_')
                tbcomm += [[ name, p_comm[nm] ]]
        # ahora si el resto
        for nm in p_comm.keys():
            if not nm.startswith('sem_'):
                name = '\\texttt{%s}'%nm.replace('_', u'\_')
                tbcomm += [[ name, p_comm[nm] ]]

        # build tex table
        TexTab_comm = tabulate(tbcomm, tablefmt='latex', headers='firstrow')

        #--- ahora hacemos la tex-tabla para los parametros diferentes
        header = [u'name \\texttt{\\textbackslash} ID',]
        for myid in self.idlist:
            header += [ '%03d'%myid ]

        tbdiff += [ header ]
        for nm in p_diff.keys():
            pars = []
            for myid in self.idlist:
                fname_inp = self.ps['dir_src'] + '/' + self.fprefix + '%03d.h5' % myid
                with h5py.File(fname_inp, 'r') as f:
                    par = f['psim/'+nm].value
                    pars += [ '%2.2e' % par ]
            tbdiff += [ ['\\texttt{$%s$}'%nm.replace('_','\_'),] + pars ]
        
        TexTab_diff = tabulate(tbdiff, tablefmt='latex', headers='firstrow')

        #--- beginin of .tex document
        #with open(os.environ['HOME']+'/utils/tex_begin.txt', 'r') as f:
        with open('./shared/tex_begin.txt', 'r') as f:
            tex_begin_lines = f.readlines()

        self.fname_tab_base = fname_tab_base = '_tmp.table_' + fname_base
        f = open(fname_tab_base+'.tex', 'w')
        for line in tex_begin_lines:
            f.write(line)
        # table of common params
        f.write(u'\\\\ \n {\\bf Common sim-parameters} \\\\ \n')
        for line in TexTab_comm:
            f.write(line)
        # table of different params
        f.write('\\vspace{1cm} \n')
        f.write(u'\\\\ \n {\\bf Different sim-parameters} \\\\ \n')
        for line in TexTab_diff:
            f.write(line)

        f.write('\n\end{document}')
        f.close()
        #--- end of .tex document

        cmd = 'pdflatex --interaction=nonstopmode {fname}'.format(fname=fname_tab_base+'.tex')
        return os.system(cmd), fname_tab_base+'.pdf'

    def compare_psim(self):
        """ Compare simulation-parameters to identify which
        are the same in each file, and which are different.

        output:
        - p_comm    : parameters in common
        - p_diff    : parameters different from one file to another
        """

        p_comm = {} # dict de params iguales
        p_diff = {} # dict de parametros en q difieren
        
        fname_inp_h5_ = self.ps['dir_src'] + '/' + self.fprefix + '%03d.h5' % self.idlist[0] # pick one
        f = h5py.File(fname_inp_h5_, 'r')
        p_test = {} # dict de prueba, para comparar si todos son iguales
        for pnm in f['psim'].keys():
            p_test[pnm] = [ f['psim/'+pnm].value ] 

        for myid in self.idlist:
            fname_inp_h5 = self.ps['dir_src'] + '/' + self.fprefix + '%03d.h5' % myid
            f = h5py.File(fname_inp_h5, 'r')
            print " ------- " + fname_inp_h5 + " ------- "
            for pnm in f['psim'].keys():
                if pnm in ('th', 'mu'):
                    continue

                fpar = f['psim/'+pnm].value # parameter from file
                if fpar == p_test[pnm]:
                    p_comm[pnm] = fpar
                else:
                    p_diff[pnm] = fpar

        print " ############ pars in common:"
        for nm in p_comm.keys():
            print nm, p_comm[nm]
        print " ############ pars different:"
        for nm in p_diff.keys():
            print nm, p_diff[nm]

        self.pars = {}
        self.pars['common'] = p_comm
        self.pars['different'] = p_diff

        return p_comm, p_diff


class GralPlot(object):
    """
    - The main idea of this class is to read data from a file (which
    can be some simulation or observation, in whatever format) and 
    grab it as a member.
    - In gral, it will handle:
        - labels
        - titles
        - check any consistencies
    - This member data can later be processed and plotted anyway we want.
    - Every plot__*() method implemented below is supposed to return
    two objects:
        - matplotlib.pyplot.figure
        - one (or more) instance(s) of "fig.subplot(X,Y,Z)"
    """
    def __init__(self,):
        self.data = np.random.random((10,100))

    def plot__parabolic(self, ):
        # grab all the rows into one single vector, putting one after another
        x = m.reshape(1, m.size)[0]
        y = x*x
        fig = figure(1, figsize=(6,4))
        ax  = fig.add_subplot(111)
        ax.plot(x, y, 'b-')
        ax.set_title('some parabolic curve')
        ax.set_xlabel('this x label')
        return fig, ax

    def plot__scatter(self, ):
        x = self.data[:,0]
        y = self.data[:,1]
        fig = figure(1, figsize=(6,4))
        ax  = fig.add_subplot(111)
        ax.plot(x, y, 'b-')
        ax.set_title('this title')
        ax.set_xlabel('some X label')
        return fig, ax




#EOF
