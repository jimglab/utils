#!/bin/bash


###########################################
### NETWORK
###########################################


# Function to get the network interface name associated
# to a specific Docker container. It takes into account
# two cases:
# - when using some network created directly by Docker (those
#   that appear in `docker network ls`).
# - when container is using another container's network.
#
docker_network_iface_from_container(){
    local CNAME="$1"
    local SUB_CNAME
    #--- setup firewall logs (for input and output traffic)
    #echo -e "\n [*] grabbing network interface used by the container..."
    local CNETWORK_ID       # network id of the container
    local CNETWORK_NAME     # container-network name
    local IFACE             # network interface used by the container

    CNETWORK_ID=$(docker container inspect $CNAME | grep NetworkID | cut -d':' -f2 | tr '"' ' ' | cut -d' ' -f3 | cut -c 1-12)
    if [[ ${CNETWORK_ID} == "" ]]; then
        # capture the *true* network associated to a container, which is 
        # mounted on another container's network.
        SUB_CNAME=$(docker container inspect $CNAME | grep NetworkMode | cut -d':' -f3 | awk '{print substr($0,0,12)}')
        CNETWORK_ID=$(docker container inspect ${SUB_CNAME} | grep NetworkID | cut -d':' -f2 | tr '"' ' ' | cut -d' ' -f3 | cut -c 1-12)
    fi
    [[ ${CNETWORK_ID} == "" ]] && return 1

    #--- just a normal container
    CNETWORK_NAME=$(docker network ls | grep $CNETWORK_ID | awk '{print $2}')
    # get the interface
    IFACE=$(docker network inspect ${CNETWORK_NAME} | grep "network.bridge.name" | awk '{print $2}' | sed 's/"\|,//g')
    echo $IFACE

    # check that this is a valid interface
    #ifconfig $IFACE > /dev/null || exit 1
}

#EOF
